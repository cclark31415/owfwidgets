google.load('feeds', '1');

function initialize () {
    setInterval(getFeed(), 30*60*1000);
}

function getFeed(){
    var feed = new google.feeds.Feed('https://www.engadget.com/rss.xml');
    feed.load(function (result) {
        if (!result.error) {
            var feedDiv = document.getElementById('feed');
            var description = result.feed.title;
            var h3 = document.createElement('h3');
            h3.appendChild(document.createTextNode(description));
            document.getElementById('description').appendChild(h3);
            for (var i = 0; i < 5; i++) {
                var entry = result.feed.entries[i];
                var div = document.createElement('div');
                var a = document.createElement('a');
                var linkText = document.createTextNode(entry.title);
                a.appendChild(linkText);
                a.title = entry.title;
                a.href = entry.link;
                a.target = '_blank';
                div.appendChild(a);
                feedDiv.appendChild(div);
            }
        }
    });
}
google.setOnLoadCallback(initialize);

